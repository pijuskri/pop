
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GeneralTest {

    static Main main;
    @BeforeAll
    static void Setup()
    {
        main = new Main();
        generate.percentChanceToSpawnPop = 50;
        main.codeSetup();
        Tile.popRequirementForCountry = 2;
        Tile.techRequirementForCountry = 0.2d;
        for(int i=0;i<30000;i++)
        {
            main.map.Calculate();
        }
    }
    @Test
    void TotalPopulationAboveEquals0Test()
    {
        for(Tile tile:main.map) assertTrue(tile.pop.total>=0);
    }
    @Test
    void TotalPopulationAddsUpTest()
    {
        for(Tile tile:main.map) assertEquals(tile.pop.total, tile.pop.farmers + tile.pop.workers + tile.pop.scientists);
    }
    @Test
    void CountryCapitalMatchesTest()
    {
        for(Tile tile:main.map) assertTrue(!tile.isCapital || tile.country.capital==tile);
    }
    @Test
    void AllCapitalsHaveACountryTest()
    {
        for(Tile tile:main.map) assertFalse(tile.isCapital && tile.country==null);
    }

}
