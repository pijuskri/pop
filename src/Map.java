import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Map implements Iterable<Tile>, Iterator<Tile> {
    public Tile[][] map;
    public List<Country> countries;
    int height;
    int width;
    public Map(int height, int width)
    {
        this.height = height;
        this.width =width;
        map = new Tile[height][width];
        countries = new ArrayList<>();
    }
    public Tile get(int y, int x)
    {
        return map[y][x];
    }
    public Tile getSafe(int y, int x)
    {
        if(y >= height) y = height-1;
        else if(y < 0) y = 0;
        if(x >= width) x = width-1;
        else if(x < 0) x = 0;
        //System.out.println(y+" "+x);
        return map[y][x];
    }
    public void set(int y, int x, Tile tile)
    {
        if(y > height) y = height;
        if(y < 0) y = 0;
        if(x > width) x = width;
        if(x < 0) x = 0;
        map[y][x] = tile;
    }

    int counter = 0; // for executing certain function only every few turns
    int turnCount = 5;
    void Calculate()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if(get(y,x).terrain!=2)
                {
                    if(counter>=turnCount) get(y,x).IntervalCalculate();
                    else get(y,x).Calculate();

                }
            }
        }
        if(counter>=turnCount) counter = 0;
        else counter++;
    }



    private int iteratorX;
    private int iteratorY;

    @Override
    public Iterator<Tile> iterator() {
        iteratorY = 0;
        iteratorX = 0;
        return this;
    }

    @Override
    public boolean hasNext() {
        return iteratorY<height && iteratorX<width;
    }

    @Override
    public Tile next() {
        iteratorX++;
        if(iteratorX>=width)
        {
            iteratorX = 0;
            iteratorY++;
        }
        return get(iteratorY,iteratorX);
    }

    public List<Tile> asList()
    {
        List<Tile> out = new ArrayList<>();
        for (int y = 0; y < height; y++)
        {
            out.addAll(Arrays.asList(map[y]).subList(0, width));
        }
        return out;
    }
}
