import java.awt.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Country {
    Tile capital;
    List<Tile> territory;
    int military = 0, infrastructure = 1, population = 0;
    double food=0, money=0, spendingCoe = 0, infraRatio = 0.1, taxRate = 0, fightingCapability = 0, aggressiveness = 0;
    String name;
    Color mapColor;
    Random random;
    List<Tile> border;
    List<Diplomacy> relations;

    public Country(Tile capital, String name)
    {
        this.capital = capital;
        this.name = name;
        territory = new ArrayList<>();
        random = new Random();
        spendingCoe = random.nextDouble();
        taxRate = random.nextDouble() % 0.2d + 0.05;
        aggressiveness = random.nextDouble()%0.5+0.01d;
        mapColor = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
        relations = new ArrayList<>();
        calculateBorder();
    }

    public void Calculate()
    {
        Taxes();
        Upkeep();
    }
    public void IntervalCalculate()
    {
        FoodToCapital();
        census();
        calculateFightingCapability();
        //InfluenceGrowth();
        Loyalty();
        TechSpread();
        Invest();
        militaryAction();
        Infrastructure();
        manageDiplomacy();
    }

    public void FoodToCapital()
    {
        food = 0;
        for(Tile tile:territory)
        {
            if(tile.food>tile.pop.total)
            {
                double transfer = (tile.food-tile.pop.total) / ((Main.distance(capital,tile) + 2));
                if(transfer>0)
                {
                    food+=transfer;
                    tile.food-=transfer;
                }

            }
        }

    }

    void Loyalty()
    {
        for(int i = 0;i<territory.size();i++)
        {
            Tile tile = territory.get(i);
            tile.loyalty = 50;
            tile.loyalty+=(fightingCapability-5)/2 + infraRatio*3 - tile.pop.total/2d - Main.distance(capital,tile)/2 - taxRate*5 + capital.pop.total/5d;
            if(tile.pop.total==0) tile.loyalty-=20;
            if(money<0) tile.loyalty-=10;

            for(Tile border:tile.neighbours)
            {
                if(border.country==this)tile.loyalty+=3;
            }

            if(tile.isCapital) tile.loyalty = 100;
            else if(tile.loyalty>100)tile.loyalty = 100;
            else if(tile.loyalty<0)
            {
                tile.loyalty = 50;
                tile.changeCountry(null);
                i--;
            }
        }

    }

    public void Taxes()
    {
        for(Tile tile:territory)
        {
            double tax = taxRate * tile.money/100;
            money+=tax * Math.sqrt(infraRatio);
            tile.money-=tax;
        }
        double tax = capital.money/100 * taxRate;
        money+=tax + 0.1;
        capital.money-=tax;
    }
    public void Upkeep()
    {
        int toRemove = 0;
        money-=(military-10)/(50d*Math.sqrt(infraRatio));
        if(money<0 && military>0)
        {
            toRemove = (int)Math.max(Math.floor(-money*2),military);
            military-=toRemove;
            money+=toRemove/2d;
        }
        money-=(infrastructure-5)/100d;
        if(money<0 && infrastructure>0)
        {
            toRemove = (int)Math.min(Math.floor(-money/6),infrastructure);
            infrastructure-=toRemove;
            money+=toRemove*6;
        }
    }
    public void Invest()
    {
        if(money>10+military/4d+infrastructure/4d)
        {
            double cost = 0;
            int toAdd = 0;
            if ( spendingCoe<random.nextDouble()) //military
            {
                toAdd = (int)Math.floor(money/6);
                if(toAdd+military>population) toAdd = population-military;
                cost += toAdd * 3;
                military += toAdd; // /2
                //for(int i=0;i<toAdd;i++){ if(money>0 && military>0) attack();}
            }
            else { //investment
                toAdd = (int)Math.floor(money/20);
                cost += toAdd * 10;
                infrastructure+=toAdd;
            }
            money -= cost;
        }
    }
    void militaryAction()
    {
        int count = random.nextInt((int)(military*aggressiveness/50 + 2));
        for(int i = 0;i<count;i++)
        {
            if (money > 0 && military > 0 && fightingCapability > 0) attack();
            else break;
        }
    }
    void manageDiplomacy()
    {
        //if(relations.size()>0) Diplomacy
        if(atWarWithCount()>=1) return;
        for (Diplomacy relation:relations)
        {
            if( relation.relation == Relation.Peace)
            {
                declareWar(relation);
                break;
            }
        }
    }
    void calculateFightingCapability()
    {
        fightingCapability = military * infraRatio;
        if(money>0)fightingCapability += Math.sqrt(money)/4;
        fightingCapability /= population*0.01d + territory.size() * 0.15d + 1;
    }
    void attack()
    {
        Tile tile = null;
        if(border.size()>0)
        {
            tile = border.get(random.nextInt(border.size()));
            if(powerRatio(tile)<0.7d || relationWithTile(tile) == Relation.Peace) tile=null;
        }
        if(tile!=null && tile.terrain!=2)
        {
            tile.recentBattle = 20;
            military -= Math.max(tile.pop.total/2,0);
            if(tile.country!=null) tile.country.military -= Math.max(tile.pop.total/4,0);
            if(battle(tile))
            {
                tile.changeCountry(this);
            }
        }
    }
    boolean battle(Tile site)
    {
        double power=site.militaryPresence(this),powerOther=site.militaryPresence(null);

        if(power>random.nextDouble() * powerOther*2)
        {
            return true;
        }
        return false;
    }
    double powerRatio(Tile site)
    {
        return site.militaryPresence(this) / site.militaryPresence(null);
    }
    private Relation relationWithTile(Tile tile)
    {
        int index = -1;
        index = relations.indexOf(new Diplomacy(tile.country));
        if(index>=0) return relations.get(index).relation;
        else return Relation.War;
    }
    Diplomacy findRelation(Country country)
    {
        int index = -1;
        index = relations.indexOf(new Diplomacy(country));
        if(index>=0) return relations.get(index);
        else return null;
    }
    int atWarWithCount()
    {
        int sum = 0;
        for (Diplomacy relation:relations)
        {
            if(relation.relation==Relation.War)sum++;
        }
        return sum;
    }
    void declareWar(Diplomacy relation)
    {
          Diplomacy relationWithThis = relation.country.findRelation(this);
          if(relationWithThis!=null) relationWithThis.relation = Relation.War;
          else {
              relation.country.calculateBorder();
              relationWithThis = relation.country.findRelation(this);
              if(relationWithThis!=null) relationWithThis.relation = Relation.War;
              else System.out.println("Diplomacy machine broke");
          }
          relation.relation = Relation.War;
    }

    /*
    boolean defend(Tile tile) // response to an attack
    {
        int chance = 10;
        chance+=tile.pop.total*2;
        chance+=fightingCapability;
        if(tile.isCapital) chance += 1000;
        if(military>tile.pop.total/2 && chance>random.nextInt(200))
        {
            military -= tile.pop.total/2;
            money -= tile.pop.total/2d;

            return true;
        }
        else return false;

    }
    */

    @Deprecated
    public void InfluenceGrowth()
    {
        double chance = 0;
        chance+=capital.tech*5;
        chance+=Math.sqrt(capital.money)/3;
        if(capital.attract>0)chance+=capital.attract/3;
        chance-=territory.size()/5d;
        chance+=military;
        chance+=infraRatio*10;

        for(Tile tile : Border())
        {
            //Random random = new Random(LocalTime.now().toNanoOfDay()*(long)tile.popgrowth);
            double localChance = chance-(Main.distance(capital,tile)*2);
            if(tile.isCapital) localChance-=10;
            if(tile.country!=null) localChance-=tile.country.military;
            if(localChance>random.nextInt(2000))
            {
                if(tile.isCapital)
                {
                    tile.isCapital = false;
                    for(Tile change:tile.country.territory) change.country = this;
                    territory.addAll(tile.country.territory);
                }
                territory.add(tile);
                //if(tile.country!=null)tile.pop--;
                tile.country = this;

            }
        }
    }

    public void TechSpread()
    {
        for(Tile tile:territory)
        {
            if(tile.terrain!=2)
            {
                double diff = capital.tech-tile.tech;
                diff*=infraRatio;
                if(diff>0) tile.tech+=(double)(diff/(double)1000);
            }
        }
    }
    public void census()
    {
        population = 0;
        population+=capital.pop.total;
        for(Tile tile:territory)
        {
            population+=tile.pop.total;
        }
    }

    void Infrastructure()
    {
        infraRatio = 3*(double)infrastructure/(population+1) + 0.2;
    }

    private List<Tile> Border()
    {
        List<Tile> border = new ArrayList<>();
        territory.add(capital);
        for(Tile influence:territory)
        {
            for(Tile tile : influence.neighbours)
            {
                if( (tile.terrain!=2 || influence.terrain!=2) && (tile.country==null || !tile.country.name.equals(name)))
                {
                    border.add(tile);
                }
            }
        }
        territory.remove(capital);

        return border;
    }
    private List<Country> neighbours()
    {
        List<Country> countries = new ArrayList<>();
        for (Tile tile:border)
        {
            if(tile.country!=null)
            {
                if(!countries.contains(tile.country)) countries.add(tile.country);
            }
        }
        return countries;
    }
    void calculateBorder()
    {
        border = Border();
        List<Country> neighbours = neighbours();
        for(int i=0;i<relations.size();i++)
        {
            if(!neighbours.contains(relations.get(i).country)) {relations.remove(i); i--;}
        }
        for(Country neighbour:neighbours)
        {
            Diplomacy temp = new Diplomacy(neighbour);
            if(!relations.contains(temp)) relations.add(temp);
        }
    }
}
