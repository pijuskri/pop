import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class view
{
	PApplet proc;
	Main main;
	public boolean isometric = false;
	public int scale = 10;
	public int windowsizeToMap = 10;
	public int zoom = 100;
	public float xOffset = 0, yOffset = 0;
	PImage tileSprite;
	PImage tileSpriteScaled;
	PImage hillTileSprite;
	PImage hillTileSpriteScaled;
	PImage tileOutlineSprite;
	PGraphics output;
	view(PApplet proc, Main main)
	{
		this.proc = proc;
		this.main = main;
	}
	
	void calculateScale()
	{
		scale = windowsizeToMap * zoom / 100;
	}

	void terrainMap(Tile tile)
	{
		float a = 0, b = 0, c = 0;
		if (tile.terrain == 0)
		{
			a = 0;
			b = 0;
			c = 0;
		} else if (tile.terrain == 1)
		{
			a = 51;
			b = 153;
			c = 51;
		} else if (tile.terrain == 2)
		{
			a = 0;
			b = 102;
			c = 204;
		} else if (tile.terrain == 3)
		{
			a = 237;
			b = 201;
			c = 175;
		} else if (tile.terrain == 4)
		{
			a = 191;
			b = 191;
			c = 191;
		}
		drawSquare(a,b,c,tile.y,tile.x);
	}

	void popMap(Tile tile)
	{
		//int maxpop = main.maxMapPop();
		int maxpop = 10;
		double change=255;
		if(maxpop>0)change = 255 / maxpop;

		float r = 255, g = 255, b = 255;
		if (tile.pop.total >= 1)
		{
			g = g - 10;
			b = b - 10;
		}
		//proc.blendMode(proc.BLEND);
		g = (int)(g - change * tile.pop.total);
		b = (int)(b - change * tile.pop.total);
		if(tile.pop.total<0) r = 0; //for debug

		drawSquare(r,g,b,tile.y,tile.x);
	}

	void resourceMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;
		if (tile.terrainFeature == TerrainFeature.Empty)
		{
			r = 255;
			g = 255;
			b = 255;
		}
		else if (tile.terrainFeature == TerrainFeature.Wheat)
		{
			r = 245;
			g = 222;
			b = 179;
		}
		else if (tile.terrainFeature == TerrainFeature.Iron)
		{
			r = 212;
			g = 215;
			b = 217;
		}
		else if (tile.terrainFeature == TerrainFeature.Forest)
		{
			r = 76;
			g = 162;
			b = 0;
		}
		else if (tile.terrainFeature == TerrainFeature.Fish)
		{
			r = 0;
			g = 102;
			b = 204;
		}
		else if (tile.terrainFeature == TerrainFeature.Jungle)
		{
			r = 21;
			g = 230;
			b = 0;
		}
		else if (tile.terrainFeature == TerrainFeature.Taiga)
		{
			r = 0;
			g = 163;
			b = 65;
		}
		drawSquare(r,g,b,tile.y,tile.x);
	}

	void attractMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;
		if (tile.attract > 0)
		{
			r -= tile.attract * 4;
			b -= tile.attract * 4;
		}
		if (tile.attract < 0)
		{
			g -= tile.attract * 4;
			b -= tile.attract * 4;
		}

		drawSquare(r,g,b,tile.y,tile.x);
	}

	/*
	void prosperMap()
	{

		for (int i = 0; i < main.w; i++)
		{
			for (int d = 0; d < main.h; d++)
			{
				float a = 255, b = 255, c = 255;
				if (tile.prosperity == "plague")
				{
					b = 0;
					c = 0;
				}
				if (tile.prosperity == "boom")
				{
					a = 0;
					c = 0;
				}
				proc.fill(a, b, c);
				proc.noStroke();
				proc.rect(d * windowsizeToMap, i * windowsizeToMap, windowsizeToMap, windowsizeToMap);
			}
		}
	}
	*/

	void tempMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		if (tile.temperature > 0)
		{
			b -= (float) (tile.temperature * 5);
			g -= (float) (tile.temperature * 5);
		}

		if (tile.temperature < 0)
		{
			r += (float) (tile.temperature * 5);
			g += (float) (tile.temperature * 5);
		}

		drawSquare(r,g,b,tile.y,tile.x);
	}
	void techMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		r = (float)(r - 10 * tile.tech);
		g = (float)(g - 10 * tile.tech);

		drawSquare(r,g,b,tile.y,tile.x);
	}
	void moneyMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;
		g = (float)(g - 0.4 * tile.money);
		b = (float)(b - 0.4 * tile.money * 6.375 );//6.375

		drawSquare(r,g,b,tile.y,tile.x);
	}
	void infoMap()
	{
		double averagePop, averageTech,averageMaxPop, averageFood,averageMoney,popcount=0,sumTech=0, sumPop=0,sumMaxPop=0,sumFood=0,sumMoney=0;
		for (int i = 0; i < main.h; i++)
		{
			for (int d = 0; d < main.w; d++)
			{
				Tile tile = main.map.get(i,d);
				if(tile.pop.total>0) popcount++;
				sumPop += tile.pop.total;
				sumTech += tile.tech;
				sumMaxPop += tile.maxpop;
				sumFood += tile.food - tile.pop.total;
				sumMoney += tile.money;
			}
		}
		proc.fill(0);
		averagePop = sumPop / popcount;
		averageTech = sumTech / popcount;
		averageMaxPop = sumMaxPop / popcount;
		averageFood = sumFood / popcount;
		averageMoney = sumMoney / popcount;
		proc.text("pop average: " + (float)averagePop, 50, 100);
		proc.text("pop total: " + (float)sumPop, 50, 150);
		proc.text("tech average: " + (float)averageTech, 50, 200);
		proc.text("maxpop average: " + (float)averageMaxPop, 50, 250);
		proc.text("food average: " + (float)averageFood, 50, 300);
		proc.text("money average: " + (float)averageMoney, 50, 350);
		proc.text("turns: " + Main.turnCounter, 50, 400);
		proc.text("turns per second: " + Main.turnsPerSecond, 50, 450);
	}
	void nationalityMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		if( tile.pop.total>0)
		{
			r = (255 / main.startingPopCount) * tile.nationality;
			g = 255 - (255 / main.startingPopCount) * tile.nationality;
			b = (255 / main.startingPopCount) * tile.nationality;
		}
		drawSquare(r,g,b,tile.y,tile.x);
	}
	void waterfallMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		r = 255- ((tile.waterfall)/(float)4);
		g = 255- ((tile.waterfall)/(float)4);

		drawSquare(r,g,b,tile.y,tile.x);
	}
	void heightMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;
		int elavation = tile.elavation;
		if(elavation<0) elavation = 0;

		r = (float)Math.pow(elavation,0.9)+20;
		g = (float)Math.pow(elavation,0.9)+20;
		b = (float)Math.pow(elavation,0.9)+20;

		drawSquare(r,g,b,tile.y,tile.x);
	}
	void fertilityMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		r = (float)tile.fertility/2;
		g = (float)tile.fertility/2;
		b = (float)tile.fertility/2;
		if(tile.fertility<100) {r=255;}

		drawSquare(r,g,b,tile.y,tile.x);
	}
	public void CountryMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;

		if(tile.country!=null)
		{
			Country tempCountry = tile.country;
			if(!main.infoDisplayed || main.clickedTile.country==null || tempCountry.equals(main.clickedTile.country))
			{
				r = tempCountry.mapColor.getRed();
				g = tempCountry.mapColor.getGreen();
				b = tempCountry.mapColor.getBlue();
			}
			else
			{
				r = tempCountry.mapColor.getRed();
				g = tempCountry.mapColor.getGreen();
				b = tempCountry.mapColor.getBlue();
				r +=150;
				g +=150;
				b +=150;
			}
		}

		if(tile.isCapital && main.infoDisplayed && main.clickedTile.country!=null && tile.country.equals(main.clickedTile.country)) {r=255;b = 0;g=0;}
		drawSquare(r,g,b,tile.y,tile.x);
	}
	public void BuildingMap(Tile tile)
	{
		float inc = 255/(float)main.buildings.size();
		float r = 255, g = 255, b = 255;

		r = 255 - (inc * tile.buildings.size() * 0.3f);
		g = 255 - (inc * tile.buildings.size() * 0.5f);
		b = 255 - (inc * tile.buildings.size());
		drawSquare(r,g,b,tile.y,tile.x);
	}
	public void BattleMap(Tile tile)
	{
		float r = 255, g = 255, b = 255;
		if(tile.recentBattle>0) {g = 0;b = 0;}
		drawSquare(r,g,b,tile.y,tile.x);
	}
	void tileInfo(Tile tile)
	{
		int sizex = 300;
		int sizey = 150;
		float y,x;

		if(isometric) {
			outline(tile.y, tile.x);

			Cord c = toIsometric(tile.y, tile.x);
			y = c.y - sizey;
			x = c.x + 5;
		}
		else {
			y = (int)( tile.y*scale-sizey);
			x = (int)(tile.x*scale);
			//drawSquare(0, 0,0, y, x);
			proc.fill(0);
			proc.stroke(0);
			proc.rect(x,y+sizey,scale,scale);
		}



		//proc.fill(0);
		//proc.rect(x,y+sizey,scale,scale);
		//drawSquare(0, 0,0, tile.x, tile.y);
		proc.fill(255,255,255);
		if(x+sizex>proc.width) x -= sizex;
		if(y<0) y+=sizey+scale;
		proc.rect(x,y,sizex,sizey);

		proc.fill(0);

		if(main.mapMode==12 && tile.country!=null) countryInfoText(tile,x,y);
		else tileInfoText(tile,x,y);

	}
	void tileInfoText(Tile tile, float x, float y)
	{
		proc.text(tile.toString(),x+2,y,300,150);
		/*
		proc.text("coords:" + tile.y +" "+ tile.x, x,y+10);
		proc.text("pop:" + tile.pop.total,x+80,y+10);
		proc.text("terrain:" + tile.terrain,x+130,y+10);
		proc.text("maxpop:" + tile.maxpop,x+200,y+10);

		proc.text("growth:" + (int)tile.popgrowth,x+5,y+30);
		proc.text("food:" + String.format("%.2f",tile.food),x+60,y+30);
		proc.text("fertility:" + String.format("%.1f",tile.fertility) + "%",x+120,y+30);

		proc.text("pos food:" + String.format("%.2f", tile.possibleFood),x+5,y+50);
		proc.text("attract:" + (int)tile.attract,x+90,y+50);
		proc.text("tech:" + String.format("%.2f",tile.tech),x+145,y+50);
		proc.text("money:" + (int)tile.money,x+200,y+50);

		proc.text("farmer:" + tile.pop.farmers,x+5,y+70);
		proc.text("worker:" + tile.pop.workers,x+55,y+70);
		proc.text("scientist:" + tile.pop.scientists,x+120,y+70);
		proc.text("buildNR:" + tile.buildings.size(),x+200,y+70);


		proc.text("height:" + tile.elavation,x+5,y+90);
		proc.text("foodCap:" + String.format("%.2f",tile.foodCap),x+70,y+90);
		proc.text("food/farmer:" + String.format("%.1f",tile.foodPerFarmer),x+150,y+90);
		 */
	}
	void countryInfoText(Tile tile, float x, float y)
	{
		Country country = tile.country;
		proc.text("money:" + String.format("%.1f",country.money), x+5,y+10);
		proc.text("mil:" + country.military, x+80,y+10);
		proc.text("coe:" + String.format("%.2f",country.spendingCoe), x+130,y+10);
		proc.text("loyalty:" + String.format("%.1f",tile.loyalty), x+5,y+30);
		proc.text("pop:" + country.population, x+80,y+30);
		proc.text("food:" + String.format("%.2f",country.food), x+5,y+50);
		proc.text("infra:" + String.format("%.2f",country.infraRatio), x+5,y+100);
		proc.text("taxRate:" + String.format("%.2f",country.taxRate), x+70,y+100);
		proc.text("might:" + String.format("%.0f",country.fightingCapability), x+150,y+100);
		proc.text("size:" + country.territory.size(), x+220,y+100);

	}



	float tileSize = 10;
	public void drawSquare(float r, float g, float b, float y, float x)
	{
		if(isometric) {
			Cord c = toIsometric(y, x);
			proc.tint(r, g, b);
			//output.image(tileSprite,c.x ,c.y);
			proc.image(tileSpriteScaled, c.x, c.y);
			//output.set((int)c.x,(int)c.y,tileSprite);
			//proc.image(tileSprite,c.x + xOffset - zoom*5 + 500,c.y + yOffset - zoom*5 + 500, tileSize * 0.021f * zoom, tileSize * 0.011f * zoom);
		}
		else {
			proc.fill(r,g,b);
			proc.noStroke();
			proc.rect(x * scale + xOffset, y * scale + yOffset, scale, scale);
		}


	}
	public void drawSquare(PImage tile, float y, float x)
	{
		Cord c = toIsometric(y, x);
		proc.image(tile,c.x ,c.y);
		//proc.image(tileSprite,c.x + xOffset - zoom*5 + 500,c.y + yOffset - zoom*5 + 500, tileSize * 0.021f * zoom, tileSize * 0.011f * zoom);

		//proc.fill(r,g,b);
		//proc.noStroke();
		//proc.rect(x * scale + xOffset, y * scale + yOffset, scale, scale);
	}
	void outline(float y, float x)
	{
		Cord c = toIsometric(y, x);
		proc.image(tileOutlineSprite,c.x,c.y, tileSize * 0.02f * zoom, tileSize * 0.01f * zoom);
		//proc.image(tileOutlineSprite,c.x + xOffset - zoom*5 + 500,c.y + yOffset - zoom*5 + 500, tileSize * 0.02f * zoom, tileSize * 0.01f * zoom);
	}
	Cord top = new Cord(0,500);
	Cord toIsometric(float y, float x)
	{
		Cord ret = new Cord(y,x);
		ret.y = (top.y + (x + y) * tileSize * 0.5f) * zoom * 0.01f;
		ret.x = (top.x + (x - y) * tileSize) * zoom * 0.01f;
		ret.y += yOffset - zoom*5 + 500;
		ret.x += xOffset - zoom*5 + 500;
		return ret;
	}
	Cord toIsometricStatic(float y, float x) {
		Cord ret = new Cord(y, x);
		ret.y = (top.y + (x + y) * tileSize * 0.5f);
		ret.x = (top.x + (x - y) * tileSize);
		return ret;
	}
	Cord fromIsometric(float y, float x)
	{
		Cord ret = new Cord(y,x);
		x -= top.x + xOffset;
		y += (zoom*5 - 500) - yOffset;
		//x += zoom*5 - 500;
		x /= tileSize * zoom * 0.01f;
		y /= tileSize * 0.5f * zoom * 0.01f;
		//x /= tileSize;
		//y /= tileSize * 0.5f;
		ret.y = (y + x)/2;
		ret.x = (y - x)/2;
		ret.y -= 0.5;
		ret.x += 0.5;

		System.out.println(ret.y + " "+ ret.x);
		return ret;
	}
}
