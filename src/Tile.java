import java.util.*;
import java.util.Map;
import processing.core.PApplet;
import processing.core.PApplet.*;

enum TerrainFeature {Empty, Wheat, Iron, Fish, Forest, Jungle, Taiga}

public class Tile
{
    Main main;
    int x, y;
    List<Tile> neighbours;
    int terrain, elavation, waterfall;
    TerrainFeature terrainFeature;
    List<ResourceLocal> resources;
    Population pop;
    int amountResource, influence,maxpop;
    double attract, popgrowth, resourceProduction, temperature, tech, food, possibleFood,money, fertility, foodCap=0, loyalty = 100, income = 0, buildingWork = 0, foodPerFarmer = 0;
    boolean isNearSea;
    String prosperity;
    List<Building> buildings;
    Building currentlyBuilding = null;
    int nationality;
    Country country = null;
    boolean isCapital = false;
    boolean builtAllBuildings=false;
    int recentBattle = 0;

    Modifiers modifiers;
    Random rand;

    static int popRequirementForCountry = 4;
    static double techRequirementForCountry = 1;

    public Tile(){}
    public Tile(int y, int x)
    {
        this.x = x;
        this.y = y;
    }
    public Tile(Main main, int y, int x)
    {
        this.main = main;
        this.x = x;
        this.y = y;
        terrain = 0;
        pop = new Population(this);
        amountResource = 0;
        terrainFeature = TerrainFeature.Empty;
        resources = new ArrayList<>();
        popgrowth = 0;
        attract = 0;
        buildings = new ArrayList<>();
        nationality = 0;
        prosperity = "neutral";
        modifiers = new Modifiers();


        rand = new Random();
    }

    public void Calculate ()
    {
        if(pop.total>0)
        {
            if(isCapital) country.Calculate();
            pops();
            //money();
            tech();
        }
    }

    public void IntervalCalculate ()
    {
        possibleFoodProduction();
        if(pop.total>0)
        {
            if(recentBattle>0) recentBattle--;
            actualFoodProduction();
            migration();
            Calculate();
            if(isCapital) country.IntervalCalculate();

            for(ResourceLocal resource: resources)
            {
                resource.Calculate();
                resource.ProductionDemandCalc();
            }


            calcMaxPop();
            attract();

            techSpread();
            chooseBuildings();
            buildBuilding();
            terrainModification();
            pop.managePromotion();
            startCountry();
        }
    }

    void possibleFoodProduction()
    {
        foodCap = (Math.pow(tech+1,0.5) * fertility/100) ;
        possibleFood = (fertility/100);

        if (terrainFeature == TerrainFeature.Forest)
            possibleFood *= 1.2;
    }
    void actualFoodProduction()
    {
        if(pop.farmers>foodCap) food = foodCap;
        else food = pop.farmers;
        food+=modifiers.foodBonus;

        double multi=0;
        multi += fertility/100;
        multi *= (modifiers.foodMult + 1);

        if (terrainFeature == TerrainFeature.Wheat)
            multi *= 2.0;
        else if (terrainFeature == TerrainFeature.Forest)
            multi *= 1.1;

        food *= multi;
        foodPerFarmer = food/pop.farmers;
        if(isCapital)food+=country.food;
    }
    void attract()
    {
        attract = 0;
        if(pop.total == 0)
        {
            if(possibleFood>=1) attract += possibleFood * 3 + 5;
            else attract-=30;
        }
        else
        {
            if(foodAfterExtraPop()>0) attract+=3;
            else attract-=50;
        }

        //attract += main.resources[terrainFeature] * 2;
        attract += tech*3;
        attract += Math.sqrt(money)/3;
        attract -= Math.pow(pop.total,1.2);
        attract += buildings.size() * 5;
        if(pop.total+1>=maxpop) attract-=30;
        if(isCapital) attract += 10;
        if(isNearSea)attract += 5;

    }
    void pops()
    {
        double multi = 1;
        if(food>pop.total) multi+= (food - pop.total)/pop.total;
        if((food<pop.total || pop.total + 1 >= maxpop || foodAfterExtraPop()<0) && tech>1) multi *= 0.1;


        popgrowth += ( multi * pop.total * 0.05f);
        if(pop.total>food) popgrowth -= (pop.total - food) * 2;

        if (popgrowth >= 100)
        {
            popgrowth = 0;
            pop.add();
        }
        else if (popgrowth < -100 && pop.total >= 1)
        {
            popgrowth = 0;
            pop.kill();
            attract();
        }
    }
    void migration()
    {
        float chance = 0;
        if(pop.total>food)
        {
            chance = (int) main.random(-100, 10*(float)(pop.total-food));
            if(chance>0) findWhereToEmigrate();
        }
        else if(pop.total>maxpop)
        {
            chance = (int) main.random(-100, 10*(pop.total-maxpop));
            if(chance>0) findWhereToEmigrate();
        }

        if(tech<2f) {
            chance = (float)( 1 + (4f-tech*2));
            if(chance>rand.nextInt(1000)) findWhereToEmigrate();
        }
    }
    void calcMaxPop()
    {
        maxpop = 10;
        maxpop += tech*2;
        maxpop += modifiers.maxpopBonus;
        if(isCapital) maxpop += 2 + Math.sqrt(country.territory.size());
        if (terrainFeature == TerrainFeature.Forest || terrainFeature == TerrainFeature.Jungle || terrainFeature == TerrainFeature.Taiga) maxpop *= 0.75;
    }

    void tech()
    {
        double techGrowth=0;
        techGrowth += (pop.total + pop.scientists*4 + 1) * 0.000005;

        if(tech>1) techGrowth /= PApplet.pow((float)(tech),1.5f);
        techGrowth *= (1+modifiers.techMult);

        tech += techGrowth;
    }
    void techSpread()
    {
        for(Tile tile:neighbours)
        {
            if(tile.terrain!=2)
            {
                double diff = tech-tile.tech;
                if(diff>0) tile.tech+= diff/6000d;
            }
        }
    }
    void money()
    {
        income = 0;

        income += pop.total * 0.0002 + pop.workers * 0.004 - pop.scientists * 0.0005;
        //income *= 5;
        income *= 0.1;
        //income -= buildings.size()/200d;

        money += income;
        if(money<0) money = 0;
    }
    void terrainModification()
    {
        if(terrainFeature == TerrainFeature.Forest && tech>3 && pop.total>3  && maxpop>=pop.total) terrainFeature = TerrainFeature.Empty;
        else if(terrainFeature == TerrainFeature.Empty && tech>4 && pop.total>2 &&  terrain == 1) terrainFeature = TerrainFeature.Wheat;
        else if(terrainFeature == TerrainFeature.Jungle && tech>4 && pop.total>3 ) terrainFeature = TerrainFeature.Empty;
    }
    void startCountry()
    {
        if(country==null && pop.total>=popRequirementForCountry && money>2 && tech>=techRequirementForCountry)
        {
            country = new Country(this,""+main.map.countries.size());
            main.map.countries.add(country);
            isCapital = true;
        }
    }

    void changeCountry(Country newCountry)
    {
        if(country!=null)
        {
            country.territory.remove(this);
            country.calculateBorder();
        }
        if(isCapital)
        {
            isCapital = false;
            for(Tile change:country.territory) change.country = newCountry;
            newCountry.territory.addAll(country.territory);
            main.map.countries.remove(country);
        }
        if(newCountry!=null)
        {
            newCountry.territory.add(this);
            newCountry.calculateBorder();
        }

        country = newCountry;
    }

    void chooseBuildings()
    {
        if(money>10 && currentlyBuilding == null)
        {
            for (Building building:main.buildings) {
                if(tech>=building.tech && money>building.cost && !buildings.contains(building) )
                {
                    money-=building.cost;
                    currentlyBuilding = building;
                    buildingWork = building.work;
                    break;
                }
            }
        }
    }
    void buildBuilding()
    {
        if(currentlyBuilding!=null)
        {
            buildingWork -= pop.workers/100d + 0.05;
            if(buildingWork<=0)
            {
                buildings.add(currentlyBuilding);
                CalculateBuildingEffect();
                currentlyBuilding = null;
            }
        }

    }

    private void CalculateBuildingEffect()
    {
        modifiers = new Modifiers();
        for (Building building:buildings) {
            modifiers.foodBonus +=building.modifiers.foodBonus;
            modifiers.foodMult +=building.modifiers.foodMult;
            modifiers.maxpopBonus +=building.modifiers.maxpopBonus;
            modifiers.techMult +=building.modifiers.techMult;
        }
    }

    void findWhereToEmigrate()
    {
        if(pop.total<=1) return;

        List<Tile> bestToMigrate =  new ArrayList<Tile>();
        List<Double> attracts =  new ArrayList<Double>();
        double max = -1000;
        double sum = 0;

        int l=(int)tech+4, lmaxy = y + l, lmaxx = x + l, lminy = y - l, lminx = x - l;
        if (lmaxx > main.w) lmaxx = main.w;
        if (lmaxy > main.h) lmaxy = main.h;
        if (lminx < 0) lminx = 0;
        if (lminy < 0) lminy = 0;

        for (int i = lminy; i < lmaxy; i++)
        {
            for (int d = lminx; d < lmaxx; d++)
            {
                Tile migrateTo = main.map.get(i,d);
                double tempAttract = migrateTo.attract;

                if(Main.distance(y, x, i, d)<=l && migrateTo.terrain!=2) {
                     tempAttract -= Math.pow((float) Main.distance(y, x, i, d), 1 + (1 / (tech / 5 + 1 + 5)));

                    //if(distance(x, y, i, d)<1.5) { tempAttract+=5; }
                    if(isCapital && migrateTo.country!=null && migrateTo.country.equals(country)) tempAttract+=10;

                    if(tempAttract>0)
                    {
                        if(max<tempAttract) max = tempAttract;
                        bestToMigrate.add(migrateTo);
                        sum+=tempAttract;
                        attracts.add(tempAttract);
                    }
                }
            }
        }
        if (max > 0)
        {
            Tile migrateTo = null;
            for (int i = 0;i<bestToMigrate.size();i++) {
                double threshold = attracts.get(i);
                if(main.random(0,(float)sum)<threshold){
                    migrateTo = bestToMigrate.get(i);
                    break;
                }
            }
            if(migrateTo==null) return;

            if(migrateTo.pop.total>0) migrateTo.tech += (tech - migrateTo.tech) * (1 / migrateTo.pop.total);
            else migrateTo.tech = tech;

            migrateTo.money += (1/pop.total)*money;
            money -= (1/pop.total)*money;

            if(pop.total==1)
            {
                migrateTo.popgrowth = popgrowth;
                popgrowth = 0;
            }

            if(migrateTo.pop.total==0) migrateTo.nationality = nationality;
            if(pop.total==0) nationality = 0;


            migrateTo.pop.add();
            pop.kill();

        }
        else pop.kill();
    }

    double foodAfterExtraPop()
    {
        double ret = food - pop.total;
        ret--;
        if(foodCap>=pop.farmers+1) ret+= foodPerFarmer;
        return ret;
    }

    public List<Tile> calculateNeighbours()
    {
        List<Tile> tiles = new ArrayList<Tile>();
        if(y+1 < main.map.height)tiles.add(main.map.map[y+1][x]);
        if(y-1 >= 0)tiles.add(main.map.map[y-1][x]);
        if(x+1 < main.map.width)tiles.add(main.map.map[y][x+1]);
        if(x-1 >= 0)tiles.add(main.map.map[y][x-1]);
        return tiles;
    }

    double militaryPresence(Country side)
    {
        double military = 0;
        if(side==null) side = country;
        if(side!=null)
        {
            military+=Math.max(side.fightingCapability - Main.distance(this,side.capital),0);
        }
        if(side==country)
        {
            military+=pop.total;
            military+=2;
            //military*=1.5;
        }
        if(military<1) military=1;
        return military;
    }

    public List<Tile> inRange(int range) {
        List<Tile> tiles = new ArrayList<Tile>();
        int l = range, lmaxy = y + l, lmaxx = x + l, lminy = y - l, lminx = x - l;
        if (lmaxx > Main.w)
            lmaxx = Main.w;
        if (lmaxy > Main.h)
            lmaxy = Main.h;
        if (lminx < 0)
            lminx = 0;
        if (lminy < 0)
            lminy = 0;

        for (int i = lminy; i < lmaxy; i++) {
            for (int d = lminx; d < lmaxx; d++) {
                if(Main.distance(this,main.map.get(i,d))<=range)
                    tiles.add(main.map.get(i,d));
            }
        }
        return tiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return x == tile.x &&
            y == tile.y &&
            terrain == tile.terrain &&
            terrainFeature == tile.terrainFeature &&
            elavation == tile.elavation &&
            waterfall == tile.waterfall &&
            fertility == tile.fertility &&
            pop == tile.pop &&
            amountResource == tile.amountResource &&
            influence == tile.influence &&
            maxpop == tile.maxpop &&
            Double.compare(tile.attract, attract) == 0 &&
            Double.compare(tile.popgrowth, popgrowth) == 0 &&
            Double.compare(tile.resourceProduction, resourceProduction) == 0 &&
            Double.compare(tile.temperature, temperature) == 0 &&
            Double.compare(tile.tech, tech) == 0 &&
            Double.compare(tile.food, food) == 0 &&
            Double.compare(tile.possibleFood, possibleFood) == 0 &&
            Double.compare(tile.money, money) == 0 &&
            isNearSea == tile.isNearSea &&
            nationality == tile.nationality &&
            prosperity.equals(tile.prosperity);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(x, y, terrain, terrainFeature, elavation, waterfall, fertility, pop, amountResource, influence, maxpop, attract, popgrowth, resourceProduction, temperature, tech, food, possibleFood, money, isNearSea, nationality, prosperity);
        result = 31 * result;
        return result;
    }
}
