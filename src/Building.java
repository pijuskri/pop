import java.util.Map;

public class Building {
    String name;
    int cost;
    double tech; // the min requirement to build the building
    int work; //work/time required to build
    Modifiers modifiers;
    public Building(String name, int cost, double techRequirement, int work)
    {
        this.name = name;
        this.cost = cost;
        this.tech = techRequirement;
        this.work = work;
        modifiers = new Modifiers();
    }
}
