public class ResourceLocal {
    Resource resource;
    Tile tile;
    double amount = 0;
    double demand=0;
    double production=0;
    double productionCost; //cost to produce single unit in money
    double demandMet = 0; //%
    double onMarket = 0; // amount in market for sale

    public ResourceLocal(Resource resource, Tile tile, double productionCost)
    {
        this.resource = resource;
        this.tile = tile;
        this.productionCost = productionCost;
        ProductionDemandCalc();
    }

    void Calculate()
    {
        amount+= production - demand;
        tile.money += Math.min(production,demand) * resource.price;
        buy();
        sell();

        demandMet = 100;
        if(amount<0) demandMet = ((-amount)/demand) * 100;
        amount = 0;
    }
    private void buy()
    {
        if(amount<0)
        {
            double toBuy = Math.max(resource.amount,-amount);
            if(toBuy*resource.price>=tile.money) toBuy = tile.money/resource.price;
            resource.amount-=toBuy;
            tile.money -= toBuy*resource.price;
            amount+=toBuy;
        }
    }
    private void sell()
    {
        double sold=0;
        if(resource.amount>0) sold = (resource.soldLast*onMarket)/resource.amount;
        onMarket-=sold;
        tile.money+=sold;

        if(amount>0)
        {
            onMarket+=amount;
            amount = 0;
        }
    }
    void ProductionDemandCalc()
    {
        demand = tile.pop.total * resource.baseConsumption;
        production = tile.pop.total * resource.baseProduction * Math.sqrt(tile.tech);
    }
}
