import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalTime;
import org.javatuples.Pair;
import processing.core.PApplet;

import java.io.Console;
import java.util.*;
import processing.core.*;
import processing.event.MouseEvent;

public class Main extends PApplet
{

	public static void main(String[] args)
	{
		PApplet.main("Main");
	}

	/// SETUP///

	public static int h = 100;
	public static int w = 100;
	public static int turnCounter = 0;

	Map map = new Map(h,w);
	List<Building> buildings;
	List<Resource> resources;
	
	public int mapMode = 0;
	generate generate = new generate(this, this);
	view view = new view(this,this);

	int startingPopCount = 0;
	public void settings()
	{
		size(1000, 1000);
		noSmooth();
	}
	public void setup()
	{
		frameRate(30);
		view.windowsizeToMap = 1000/h;
		view.tileSprite = loadImage("../images/tile.bmp");
		view.hillTileSprite = loadImage("../images/hillTile.bmp");
		view.tileOutlineSprite = loadImage("../images/tileselection.bmp");
		view.output = createGraphics(3200, 3200);

		codeSetup();

		thread("Calculate");
	}

	public void codeSetup()
	{

		for (int i = 0; i < h; i++)
		{
			for (int d = 0; d < w; d++)
			{
				map.set(i,d, new Tile(this,i,d));
			}
		}
		resources = Arrays.asList(
			new Resource("wood", ResourceType.Natural, 0.01, 0.5,0.1),
			new Resource("stone", ResourceType.Natural, 0.001, 0.2,0.1)
		);
		generate.generateWorld();
		buildings = new ArrayList<>();


		Building toAdd = new Building("Tents",20 , 0.3,10);
		toAdd.modifiers.maxpopBonus = 2;
		buildings.add(toAdd);

		toAdd = new Building("Food pit",30,0.5,10);
		toAdd.modifiers.foodBonus = 2;
		buildings.add(toAdd);

		toAdd = new Building("Granary",30,0.7,20);
		toAdd.modifiers.foodBonus = 3;
		buildings.add(toAdd);

		toAdd = new Building("Irrigation",50 , 1,20);
		toAdd.modifiers.foodMult = 0.3;
		buildings.add(toAdd);

		toAdd = new Building("Houses",50 , 1,50);
		toAdd.modifiers.maxpopBonus = 6;
		buildings.add(toAdd);

		toAdd = new Building("School",50 , 1,50);
		toAdd.modifiers.techMult = 0.3;
		buildings.add(toAdd);

		toAdd = new Building("Library",150 , 2,100);
		toAdd.modifiers.techMult = 0.4;
		buildings.add(toAdd);

		toAdd = new Building("Social housing",150 , 2,100);
		toAdd.modifiers.maxpopBonus = 6;
		buildings.add(toAdd);
	}

	//// RUN CONSTANT/////

	int calculationInterval = 100;
	int timeCalc = 0;
	int timeLast = 0;
	int timeMode = 0;

	public void draw()
	{
		if(focused) frameRate(60);
		else frameRate(10);
		Render();
	}



	LocalTime startTime;
	double time = 0;
	public static double turnsPerSecond = 0;
	public void Calculate()
	{
		startTime = LocalTime.now();
		int limitTurns = 0;
		timeControl();
		while(true)
		{
			//delay(calculationInterval);
			try
			{
				Thread.sleep(calculationInterval);
			}
			catch(InterruptedException ex)
			{
				Thread.currentThread().interrupt();
			}
			map.Calculate();
			if(map.counter==0) for(Resource resource:resources)
			{
				resource.Calculate();
			}
			turnCounter++;
			limitTurns++;
			if(limitTurns>3000) {limitTurns = 0;startTime = LocalTime.now();}
			time = Duration.between(startTime,LocalTime.now()).toMillis();
			turnsPerSecond = 1000* limitTurns/time;
		}

	}

	void Render()
	{
		//view.output = createImage(6400,6400,ARGB);
		background(0);
		view.tileSpriteScaled = view.tileSprite.copy();
		view.tileSpriteScaled.resize((int)(64*view.zoom*0.011/3.2), (int)(32*view.zoom*0.011/3.2));
		view.hillTileSpriteScaled = view.hillTileSprite.copy();
		view.hillTileSpriteScaled.resize((int)(64*view.zoom*0.011/3.2), (int)(64*view.zoom*0.011/3.2));
		/*
		for (int y = 0; y < h; y++) {
			for (int x = 0; x <= y; x++) {
				mapLoop(y,x);
			}
		}
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < x; y++) {
				mapLoop(y,x);
			}
		}
		 */
		//view.output.beginDraw();
		//view.output.background(0);
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				mapLoop(y,x);
			}
		}
		if (mapMode == 8) view.infoMap();
		//view.output.endDraw();
		//image(view.output,0,0);
		//image(view.output,0,0,view.zoom*0.01f*1000,view.zoom*0.01f*1000);

		// print(map[0][0].pop + " ");
		if(infoDisplayed)
		{
			view.tileInfo(clickedTile);
		}
		fill(0);
		textSize(14);
		textAlign(RIGHT);
		//text("turns per second: " + String.format("%.0f",Main.turnsPerSecond), width, 30);
		outlineText("turns per second: " + String.format("%.0f",Main.turnsPerSecond), width-5,30);
		textAlign(LEFT);
		textSize(11);
	}
	void mapLoop(int y, int x)
	{
		Tile tile = map.getSafe(y,x);
		if (mapMode == 0) view.terrainMap(tile);
		else if (mapMode == 1) view.popMap(tile);
		else if (mapMode == 2) view.resourceMap(tile);
		else if (mapMode == 3) view.attractMap(tile);
		else if (mapMode == 4) view.nationalityMap(tile);
		else if (mapMode == 5) view.tempMap(tile);
		else if (mapMode == 6) view.techMap(tile);
		else if (mapMode == 7) view.moneyMap(tile);
		else if (mapMode == 9) view.waterfallMap(tile);
		else if (mapMode == 10) view.heightMap(tile);
		else if (mapMode == 11) view.fertilityMap(tile);
		else if (mapMode == 12) view.CountryMap(tile);
		else if (mapMode == 13) view.BuildingMap(tile);
		else if (mapMode == 14) view.BattleMap(tile);
	}

	void timeControl()
	{
		if (timeMode == 0)
		{
			calculationInterval = 200;
		}
		if (timeMode == 1)
		{
			calculationInterval = 50;
		}
		if (timeMode == 2)
		{
			calculationInterval = 20;
		}
		if (timeMode == 3)
		{
			calculationInterval = 7;
		}
		if (timeMode == 4)
		{
			calculationInterval = 3;
		}
		if (timeMode == 5)
		{
			calculationInterval = 0;
		}
	}


	void outlineText(String text, float x, float y)
	{
		fill(0);
		for(int i = -2;i<=2;i++)
		{
			text(text,x+i,y);
			text(text,x,y+i);
			text(text,x+i,y+i);
			text(text,x+i,y+i);
		}
		fill(255);
		text(text,x,y);
	}
	public static double distance(int x, int y, int xd, int yd)
	{
		double distance = 0;
		distance = sqrt(pow((xd - x), 2) + pow((yd - y), 2) );
		//System.out.println(x +" "+ y + " " + distance +" "+ xd +" "+ yd); 
		/*
		 * int [][]r = new int[w][h]; Queue qx2 = new LinkedList(); Queue qy2 = new
		 * LinkedList(); for(int i=0;i<w;i++) { for(int d=0;d<h;d++) {
		 * 
		 * } } qx2.add(x); qy2.add(y);
		 * 
		 * while(qx.size()>0) { int xl = (int)qx.peek(); int yl = (int)qy.peek();
		 * 
		 * if(xl+1<w) generateExtension(x,y,x+1,y); if(yl+1<h)
		 * generateExtension(x,y,x,y+1); if(xl-1>=0) generateExtension(x,y,x-1,y);
		 * if(yl-1>=0) generateExtension(x,y,x,y-1);
		 * 
		 * qx.remove(); qy.remove(); }
		 */
		return distance;
	}

	public static double distance(Position a, Position b) {
		double distance = 0;
		distance = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2));
		return distance;
	}
	public static double distance(Tile a, Tile b) {
		double distance = 0;
		distance = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2));
		return distance;
	}

	//// MAPS////

	boolean infoDisplayed = false;
	Tile clickedTile = null;

	public void mousePressed()
	{
		Cord c = view.fromIsometric(mouseY,mouseX);
		if(!view.isometric) c = new Cord(mouseX/view.windowsizeToMap,mouseY/view.windowsizeToMap);
		Tile tile = map.get((int)c.x,(int)c.y);
		println(tile.y +" "+ tile.x);
		if(!infoDisplayed)
		{
			//System.out.println(mouseY/view.windowsizeToMap+" "+mouseX/view.windowsizeToMap);
			clickedTile = tile;
			infoDisplayed = true;
		}
		else if(clickedTile.equals(tile)) infoDisplayed = false;
		else clickedTile = tile;
	}

	public void keyPressed()
	{
		switch (key)
		{
			case '1':
				mapMode = 0;
				break;
			case '2':
				mapMode = 1;
				break;
			case '3':
				mapMode = 2;
				break;
			case '4':
				mapMode = 3;
				break;
			case '5':
				mapMode = 4;
				break;
			case '6':
				mapMode = 5;
				break;
			case '7':
				mapMode = 6;
				break;
			case '8':
				mapMode = 7;
				break;
			case '9':
				mapMode = 8;
				break;
			case 'w':
				mapMode = 9;
				break;
			case 'h':
				mapMode = 10;
				break;
			case 'f':
				mapMode = 11;
				break;
			case 'c':
				mapMode = 12;
				break;
			case 'b':
				mapMode = 13;
				break;
			case 't':
				mapMode = 14;
				break;
			case 'i':
				view.isometric=!view.isometric;
				break;
			case '+':
				if (timeMode < 5) {
					timeMode++;
					timeControl();
				}
				break;
			case '-':
				if (timeMode > 0) {
					timeMode--;
					timeControl();
				}
				break;
		}

		if (keyCode == UP) view.yOffset+=500/view.windowsizeToMap;
		else if (keyCode == DOWN) view.yOffset-=500/view.windowsizeToMap;
		else if (keyCode == LEFT) view.xOffset+=500/view.windowsizeToMap;
		else if (keyCode == RIGHT) view.xOffset-=500/view.windowsizeToMap;
	}
	public void mouseWheel(MouseEvent event) {
		float e = event.getCount();
		if ((e>0 && view.zoom>30) || (e<0 && view.zoom<500))
		{
			double speed = 10;
			float z = -(float)(int)e;
			z = z/20 + 1;
			view.zoom *= z;
			view.calculateScale();
		}
	}
	int maxMapPop() 
	{
		int pop=0;
		for (int i = 0; i < w; i++)
		{
			for (int d = 0; d < h; d++)
			{
				if(map.get(i,d).pop.total>pop)pop = map.get(i,d).pop.total;
			}
		}
		return pop;
	}
	static double ratio(double a, double b,double w1, double w2)
	{
		if(a==0 || b==0) return 0;
		double ret = 0;
		if(a>=b) ret = w1 * a/b;
		else ret = w2 * b/a;
		return ret;
	}
}
