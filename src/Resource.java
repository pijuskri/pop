import java.util.Objects;

public class Resource {
    String name;
    ResourceType resourceType;
    double baseprice;
    double price=0; //global
    double amount = 0; //global
    double sold=0; //this turn
    double soldLast=0; //1 turn ago
    double produced=0; //1 turn ago
    double producedLast=0; //1 turn ago
    double baseProduction; //per pop
    double baseConsumption; //per pop
    public Resource(String name)
    {

    }
    public Resource(String name, ResourceType resourceType, double baseprice, double baseProduction, double baseConsumption)
    {
        this.name = name;
        this.resourceType = resourceType;
        this.baseprice = baseprice;
        price = baseprice;
        this.baseProduction = baseProduction;
        this.baseConsumption = baseConsumption;
    }

    public void Calculate()
    {
        soldLast = sold;
        sold = 0;
        producedLast = produced;
        produced = 0;
    }

    void calculatePrice()
    {
        double ratio = 0;
        if(sold>=produced && produced>0) ratio = sold/produced - 1;
        else if(sold>0) ratio = -produced/sold + 1;

        price+= baseprice * ratio * 0.001;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource resource = (Resource) o;
        return Objects.equals(name, resource.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
enum ResourceType {Natural, Produced}
