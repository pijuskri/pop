import java.util.Objects;
 enum Relation { Peace, War };
public class Diplomacy
{

    Country country;
    Relation relation = Relation.Peace;
    public Diplomacy(Country country)
    {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diplomacy diplomacy = (Diplomacy) o;
        return Objects.equals(country, diplomacy.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country);
    }
}
