import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import processing.core.PApplet;

public class generate
{
	PApplet proc;
	Main main;
	Random rand;

	static double percentChanceToSpawnPop = 2;

	generate(PApplet proc, Main main)
	{
		this.proc = proc;
		this.main = main;
		rand = new Random();
	}

	public void generateWorld()
	{
		generateWaterfall();
		generateHeightProfile();

		generateTemperature();
		for (int i = 0; i < Main.h; i++)
		{
			for (int d = 0; d < Main.w; d++)
			{
				Tile tile = main.map.get(i,d);
				tile.neighbours = tile.calculateNeighbours();
				tile.elavation -= 450;
				tile.waterfall +=200;
				tile.waterfall*=0.8;

				double halfHeight = Main.h/2d;
				if(i<halfHeight)tile.temperature += squish(i/halfHeight)*27+8;
				else tile.temperature += squish((Main.h-i)/halfHeight)*27+8;
				//main.map.get(i,d).temperature += ((-1 * (i - main.h) * i) / 60 - 5);
				if (tile.elavation > 0)
					tile.temperature -= (tile.elavation / 20f);
				
				if (tile.elavation < 0)
					tile.terrain = 2; //water

				if (tile.elavation >= 160)
					tile.terrain = 4;
				else if (tile.elavation >= 0)
				{
					if (tile.waterfall < 200 && tile.temperature>20)
						tile.terrain = 3;
					else
						tile.terrain = 1;
				}

				/*if (main.map.get(i,d).elavation > 0 && main.map.get(i,d).temperature<0)
					main.map.get(i,d).terrain = 4;
					*/
			}
		}
		/*
		 * for(int i=0;i<5;i++) { int x, y; do { x = (int)proc.random(0,main.w); y =
		 * (int)proc.random(0,main.h); } while ( main.map[x][y].terrain == 2 );
		 * River(x,y); }
		 */
		generateFertility();
		//generateRivers();
		generateForest();

		for (int i = 0; i < main.w; i++)
		{
			for (int d = 0; d < main.h; d++)
			{
				Tile tile = main.map.get(i,d);
				tile.fertility += 55 + Math.pow(Math.max(tile.temperature-2,0),0.7)*6 + (tile.waterfall - 500)/5d;
				if(tile.fertility<0) tile.fertility = 0;

				if (tile.terrain != 2 && tile.terrain != 0)
				{
					double tempChance = 1 - percentChanceToSpawnPop/100;
					float chance = proc.random(0, 1000);
					if (chance >= 1000*tempChance)
					{
						tile.pop.add();
						tile.nationality = ++main.startingPopCount;
					}
				}
				IsNearWater(i, d);
				if(tile.terrain!=2)
				{
					tile.maxpop = 5;
					tile.possibleFoodProduction();
					tile.attract();
				}

				// wheat
				int chance = 0;
				int r = (int) proc.random(0, 100);

				if (tile.terrain == 1)
					chance = -1;
				if (tile.terrain == 2)
					chance = -1;
				if (tile.terrain == 3)
					chance = -1;
				if (tile.terrain == 4)
					chance = -1;

				if (r < chance)
				{
					tile.terrainFeature = TerrainFeature.Wheat;
					tile.amountResource = (int) proc.random(5000, 25000);
				}

				// iron
				r = (int) proc.random(0, 100);

				if (tile.terrain == 1)
					chance = 3;
				if (tile.terrain == 2)
					chance = -1;
				if (tile.terrain == 3)
					chance = 5;
				if (tile.terrain == 4)
					chance = 15;

				if (r < chance)
				{
					tile.terrainFeature = TerrainFeature.Iron;
					tile.amountResource = (int) proc.random(1000, 5000);
				}

				// fish
				r = (int) proc.random(0, 100);

				if (tile.terrain == 1)
					chance = -1;
				if (tile.terrain == 2)
					chance = 10;
				if (tile.terrain == 3)
					chance = -1;
				if (tile.terrain == 4)
					chance = -1;
				if (r < chance)
				{
					tile.terrainFeature = TerrainFeature.Fish;
					tile.amountResource = (int) proc.random(5000, 25000);
				}

				if(tile.terrainFeature == TerrainFeature.Forest || tile.terrainFeature == TerrainFeature.Jungle)
				{
					tile.resources.add(new ResourceLocal(main.resources.get(0),tile,0));
				}
				else tile.resources.add(new ResourceLocal(main.resources.get(1),tile,0));
				
			}
		}
	}
	void generateHeightProfile()
	{
		float exp=0.02f;
		int iterations = 10;
		for (int q = 0; q < iterations; q++)
		{
			exp+=0.007f;
			proc.noiseSeed((long) proc.random(q*10+10) * 100);
			float start = proc.random(100);
			float x = start;
			for (int i = 0; i < main.w; i++)
			{
				x += exp;
				float y = start;
				for (int d = 0; d < main.h; d++)
				{
					y += exp;
					main.map.get(i,d).elavation += proc.noise(x, y) * 1000 / iterations;
				}
			}
		}
	}
	public void generateTemperature() 
	{
		float exp=0.02f;
		int iterations = 5;
		for (int q = 0; q < iterations; q++)
		{
			exp+=0.004f;
			proc.noiseSeed((long) proc.random(q*10+10) * 100);
			float start = proc.random(100);
			float x = start;
			for (int i = 0; i < main.w; i++)
			{
				x += exp;
				float y = start;
				for (int d = 0; d < main.h; d++)
				{
					y += exp;
					main.map.get(i,d).temperature += (proc.noise(x, y) * 30 - 17) / iterations;
				}
			}
		}
	}
	public void generateWaterfall()
	{
		float exp=0.02f;
		int iterations = 5;
		for (int q = 0; q < iterations; q++)
		{
			exp+=0.004f;
			proc.noiseSeed((long) proc.random(q*10+10) * 100);
			float start = proc.random(100);
			float x = start;
			for (int i = 0; i < main.w; i++)
			{
				x += exp;
				float y = start;
				for (int d = 0; d < main.h; d++)
				{
					y += exp;
					main.map.get(i,d).waterfall += proc.noise(x, y) * 1000 / iterations;
				}
			}
		}
	}
	public void generateFertility()
	{
		float start = proc.random(100);
		float exp=0.03f;
		float x = 0.0f + start;
		for (int i = 0; i < main.w; i++)
		{
			x += exp;
			float y = 0.0f + start;
			for (int d = 0; d < main.h; d++)
			{
				y += exp;
				main.map.get(i,d).fertility = (int) (proc.noise(x, y) * 50);
			}
		}
		for (int q = 0; q < 5; q++)
		{
			proc.noiseSeed((long) proc.random(q*10+10) * 100);
			x = 0;
			for (int i = 0; i < main.w; i++)
			{
				x += exp;
				float y = 0.0f + start;
				for (int d = 0; d < main.h; d++)
				{
					y += exp;
					main.map.get(i,d).fertility = (int) (main.map.get(i,d).fertility + (proc.noise(x, y) * 50)) / 2d;
				}
			}
		}
	}
	void generateForest()
	{
		float exp=0.03f;
		int iterations = 5;
		for (int q = 0; q < iterations; q++)
		{
			exp+=0.015f;
			proc.noiseSeed((long) proc.random(q*10+10) * 100);
			float start = proc.random(100);
			float x = start;
			for (int i = 0; i < main.w; i++)
			{
				x += exp;
				float y = start;
				for (int d = 0; d < main.h; d++)
				{
					y += exp;
					double chance = proc.noise(x, y);
					if (chance<0.45 && main.map.get(i,d).terrain == 1)
					{
						if(main.map.get(i,d).temperature>25) main.map.get(i,d).terrainFeature = TerrainFeature.Jungle;
						else if(main.map.get(i,d).temperature<3) main.map.get(i,d).terrainFeature = TerrainFeature.Taiga;
						else main.map.get(i,d).terrainFeature = TerrainFeature.Forest;
						main.map.get(i,d).amountResource = (int) proc.random(5000, 25000);
					}
					else main.map.get(i,d).terrainFeature = TerrainFeature.Empty;
				}
			}
		}
	}

	void generateRivers()
	{
		List<Tile> high = main.map.asList();
		high.sort(Comparator.comparingInt(o -> o.elavation));

		for(int i=0;i<high.size()&&i<100;i++)
		{
			for(int d=0;d<high.size()&&d<100;d++)
			{
				if(i!=d && Main.distance(high.get(i), high.get(d))<30)
				{
					high.remove(d);
					i--;
					break;
				}
			}
		}
		int limit = 10;
		for (Tile tile : high) {
			River(tile.y, tile.x);
			limit--;
			if(limit<0)break;
		}
	}
	void River(int starty, int startx)
	{
		int x = startx;
		int y = starty;
		List<Tile> path = new ArrayList<>();
		while(true)
		{
			//System.out.println(x+" "+y);
			main.map.get(y,x).terrain = 2;
			Tile go = null;
			Tile current = main.map.get(y,x);
			path.add(current);
			//System.out.println("size:"+	current.inRange(5).size());
			int min = 9999;

			for(Tile tile:current.neighbours)
			{
				if((tile.terrain!=2 || tile.elavation<=0) && tile.elavation<min)
				{
					min = tile.elavation;
					go = tile;
				}
			}

			if(go==null)break;
			x = go.x;
			y = go.y;
			if (main.map.get(y, x).terrain == 2) break;

		}

		for(Tile current: path)
		{
			for(Tile tile:current.inRange(5))
			{
				double dist = 5 - Main.distance(current,tile);
				tile.elavation-=dist;
				tile.fertility+=dist/2;
				tile.waterfall+=dist*2;

			}
		}

	}

	void IsNearWater(int x, int y)
	{
		if(main.map.getSafe(y,x+1).terrain == 2) main.map.get(y,x).isNearSea = true;
		if(main.map.getSafe(y + 1,x).terrain == 2)  main.map.get(y,x).isNearSea = true;
		if(main.map.getSafe(y,x-1).terrain == 2)  main.map.get(y,x).isNearSea = true;
		if(main.map.getSafe(y - 1,x).terrain == 2)  main.map.get(y,x).isNearSea = true;
	}

	//actually a logistic function
	double squish(double x)
	{
		return 1/(1+Math.exp(-x*8+4));
	}

}
