import java.util.Random;
import java.util.*;

public class Population {
    int total=0;
    Tile tile;
    int farmers=0, scientists=0,  workers = 0;
    Random rand;
    public Population(Tile tile)
    {
        this.tile = tile;
        rand = new Random();
    }
    void add()
    {
        total++;
        farmers++;
    }
    void kill()
    {
        if(total<=0)System.out.println("killing nobody");
        if(farmers==0) demote();
        farmers--;
        total--;

        if(total==0 && tile.isCapital)
        {
            System.out.println("Capital died");
            tile.isCapital = false;
            for(Tile change:tile.country.territory) change.country = null;
        }
    }
    void managePromotion()
    {
        if((tile.food>total+tile.foodPerFarmer || farmers>tile.foodCap) && farmers>0)
        {
            double chance = tile.tech+1;
            if(farmers>tile.foodCap) chance+=10;
            if(chance>rand.nextInt(2000))
            {
                if(0.5>rand.nextDouble()) scientists++;
                else workers++;

                farmers--;
            }
        }

        if(tile.food<total && total>farmers && farmers+1<=tile.foodCap) demote();
    }
    void demote()
    {
        int chance = rand.nextInt(total-farmers);
        if(scientists>0 && scientists>=chance) scientists--;
        else if(workers>0) workers--;
        else System.out.println("failed to demote "+chance + " " + total);

        farmers++;
    }
}
